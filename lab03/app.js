"use strict";

var captcha = genCaptcha();

function genCaptcha() {
    var operators = ['+', '-', '*'];
    var a = Math.floor(Math.random() * 10) + 1; // Random INT from 1-10

    var b = Math.floor(Math.random() * 10) + 1; // Random INT from 1-10

    var op = operators[Math.floor(Math.random() * operators.length)]; // Number 1 (a) should always be the bigger number

    var captcha = {
        a: a > b ? a : b,
        b: a > b ? b : a,
        operator: op
    };
    var taskMsg = 'Calculate ' + captcha.a + ' ' + captcha.operator + ' ' + captcha.b;
    document.getElementById('taskMsg').innerText = taskMsg;
    return captcha;
}

function verifyCaptcha(check) {
    if (captcha.operator == '-' && captcha.a - captcha.b == check) {
        return true;
    }

    if (captcha.operator == '+' && captcha.a + captcha.b == check) {
        return true;
    }

    if (captcha.operator == '*' && captcha.a * captcha.b == check) {
        return true;
    }

    return false;
}

function evaluateResult() {
    var val = document.getElementById('mathIn').value;
    var el = document.getElementById('resultMsg');

    if (verifyCaptcha(val)) {
        el.innerText = 'correct';
        el.classList.remove('alert-danger');
        el.classList.add('alert-success');
        el.style.display = '';
    } else {
        el.innerText = 'incorrect';
        el.classList.remove('alert-success');
        el.classList.add('alert-danger');
        el.style.display = '';
    }
}

function printLoop() {
    var val = document.getElementById('loopNumber').value;
    var outputEl = document.getElementById('loopOutput');
    outputEl.innerText = '';
    var i;
    for (i = 1; i <= val; i++) {
        (function (outputEl, i) {
            setTimeout(function () {
                appendNumber(outputEl, i, val);
            }, 10 * i);
        })(outputEl, i, val);
    }
}

function appendNumber(el, i, val) {
    var tmp = i + (i == val ? '' : '<br>');
    el.insertAdjacentHTML("beforeend", tmp);
}
